# Entity Loader

A resource for modders, developped for usage on servers. On servers players have a limited amount of sectors, that are simulated (default: 5) when there is no player in the related sector. That means, in endgame a lot of player sectors are not simulated.

This mod allows you to register player/alliance owned entities to keep always updated on a server. As long as the related player is online, the sector, where a registered entity is in, will be simulated.


**This mod does not affect singleplayer game.** The amount of simulated sectors per player is only limited when playing on a server - sometimes also when playing on local network (lan), depending on your configuration. Entity Loader is designed to be used by other mods. Although you can use this mod by console commands as a standalone mod, and manually configure entities to be kept alive.

### Mod config
This mod can be configured in the galaxy folder: `moddata/ConfigLib/EntityLoader-2271143578.lua`. Default settings are a good compromise between performance impact and usability.

#### Note
Keep in mind that simulating too many sectors can cause issues. CPU and memory usage may increase. Be careful with the configs.


## Usage
The mod provides a new player script, you can use to register/unregister entities to keep their current sector updated. Players and alliances are supported.

Identifier for entities will always be the name, not the id! The context is player/alliance related, so the name is still unique.

There are 2 different methods.

### 1. Use the namespace

The easiest method, but it only works in scripts attached to the related player or alliance.

	if EntityLoader then
		local entity = Entity(Player().craftIndex) -- or Alliance()
	
		EntityLoader.addEntity(entity.name, true) -- Easy example how to add the players current craft
	end

### 2. Invoke function

When calling from arbitrary scripts, you can invoke functions.


	local faction = MyCustomFactionGetter() -- get Player or Alliance object
	local shipname = "myAwesomeTestship"

	invokeFactionFunction(faction.index, false, "EntityLoader.lua", "addEntity", shipname, true)


## EntityLoader docs

```
function getEntities()

Get all registered entities

Returns
	table of entities (custom tables, not entity objects)
```


```
function getEntity(name)

Get the related entity if registered in EntityLoader

Parameters
	name: the name of the entity or the entity itself

Returns
	table containing basic information about the related entity (if registered)
```

```	
function addEntity(name, force)

Register the entity to keep it updated

Parameters
	name: the name of the entity or the entity itself
	force: if entity limit is reached, remove oldest entity

Returns
	table containing basic information about the related entity (if successful registered)
```
	
```
function removeEntity(name)

Unregister the related entity

Parameters
	name: the name of the entity or the entity itself

Returns
	the name of the entity
```

```
function removeOldestEntity()

Unregister the entity, that was added first

Returns
	the name of the entity
```


```
function clearOverflow()

Unregister entities, until entity limit is not exceeded.
Usely you should not use this, EntityLoader will automaticly do for you.

Returns
	bool
```