package.path = package.path .. ";data/scripts/lib/ConfigLib/?.lua"
package.path = package.path .. ";data/scripts/lib/?.lua"

local ConfigLib = include("ConfigLib")
local LoaderConfig = ConfigLib("2271143578")

include("utility")
include("extutils")

-- Don't remove or alter the following comment, it tells the game the namespace this script lives in. If you remove it, the script will break.
-- namespace EntityLoader
EntityLoader = {}

-- Never load all sectors at the same time
local Queue = {}
Queue.list = {}

--[[ Ship name is our identifier, as we need it to load ship data.
local entities = {
	entity1name = {
		name = "entity1name",
		timer = @var float,
		dateAdded = int
	},
	entity2name = {...},
}
]]
local entities = {}

EntityLoader.NumMaxEntities = LoaderConfig.get("NumMaxEntities") or 5
EntityLoader.KeepAliveTime = LoaderConfig.get("KeepAliveTime") or 60
EntityLoader.KeepAliveInterval = LoaderConfig.get("KeepAliveInterval") or 30
EntityLoader.UpdateInterval = LoaderConfig.get("UpdateInterval") or 3


function EntityLoader.getUpdateInterval()
    return EntityLoader.UpdateInterval
end

function EntityLoader.initialize()
	local faction = getParentFaction()
	
	faction:registerCallback("onShipNameUpdated", "onShipNameUpdated")
end


function EntityLoader.updateServer(timestep)
	-- Keep registered entities updated regularly
	-- update entity timers
	for name,entity in pairs(entities) do
		entity.timer = entity.timer + timestep
		if entity.timer > EntityLoader.KeepAliveInterval then
			Queue.add(entity.name)
			entity.timer = 0
		end
	end
	
	local faction = getParentFaction()
	local galaxy = Galaxy()
	
	for i,name in spairs(Queue.list, function(t,a,b) return a < b end) do
		local x, y = faction:getShipPosition(name)
		local isAvailable = EntityLoader.getEntityAvailable(name)
		if isAvailable and x and y then
			galaxy:keepOrGetSector(x, y, EntityLoader.KeepAliveTime)
			LoaderConfig.log(4, "Load entity", name, "at position", x .. ":" .. y)
		elseif not isAvailable then
			EntityLoader.removeEntity(name)
		else
			LoaderConfig.log(2, "skip ship - could not determine sector:", name)
		end
		
		Queue.list[i] = nil -- remove entry from queue
		
		break 
	end
end


function EntityLoader.restore(data)
	-- Restore registered entity data
	entities = data.entities

	EntityLoader.clearOverflow()
end


function EntityLoader.secure()
	local data = {}
	
	-- secure registered entity data
	data.entities = entities
	
	return data
end


function EntityLoader.getEntityAvailable(name)
	local faction = getParentFaction()
	local entry = ShipDatabaseEntry(faction.index, name)

	if entry and entry:exists() then
		return true
	end

	return false
end


---------------------------------------
-- Callbacks --------------------------
---------------------------------------
function EntityLoader.onShipNameUpdated(name, newName)
	local entity = entities[name]
	entity.name = newName

	EntityLoader.removeEntity(name)

	entities[newName] = entity
end



---------------------------------------
-- Queue management -------------------
---------------------------------------
function Queue.add(name)
	if EntityLoader.getEntity(name) then
		table.insert(Queue.list, name)
		LoaderConfig.log(5, "Add entity to queue:", name)
	end
end



---------------------------------------
-- Entity management ------------------
---------------------------------------
function EntityLoader.getEntities()
	return entities
end

function EntityLoader.getEntity(name)
	return entities[name]
end

-- function EntityLoader.searchEntities(field, value)
-- 	-- Search entity by field and value, return table of shipnames
-- end


-- Parameter: Entity or name
function EntityLoader.addEntity(var, force)
	LoaderConfig.log(4, "addEntity", var, force)

	if force or tablelength(entities) < EntityLoader.NumMaxEntities then
		local entity = {}

		if type(var) == "string" then
			entity.name = var
		elseif type(var) == "userdata" and valid(var) then
			entity.name = var.name
		end
		entity.timer = 9999999 -- Activate now
		entity.dateAdded = os.time()
		
		if entity.name and EntityLoader.getEntityAvailable(entity.name) then
			entities[entity.name] = entity
			LoaderConfig.log(4, "Register entity:", entity.name)

			if force then
				EntityLoader.clearOverflow()
			end

			return entity
		else
			LoaderConfig.log(2, "Register entity", entity.name, "failed")
		end
	else
		LoaderConfig.log(2, "Register entity", entity.name, "failed, cause entity limit is exceeded")
	end

	
end


-- Parameter: Entity or name
function EntityLoader.removeEntity(var)
	local name
	if type(var) == "string" then
		name = var
	elseif type(var) == "userdata" and valid(var) then
		name = var.name
	end
	
	if entities[name] then
		entities[name] = nil
		LoaderConfig.log(4, "Unregister entity", name)
		
		return name
	end
end


function EntityLoader.removeOldestEntity()
	for name,entity in spairs(entities, function(t,a,b) return t[a].dateAdded < t[b].dateAdded end) do
		return EntityLoader.removeEntity(name)
	end
end


function EntityLoader.clearOverflow()
	while tablelength(entities) > EntityLoader.NumMaxEntities do
		EntityLoader.removeOldestEntity()
	end

	return true
end



