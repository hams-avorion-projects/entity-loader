package.path = package.path .. ";data/scripts/lib/?.lua"

local actions = {
	getEntities			= true,
	getEntity			= true,
	searchEntities		= true,
	addEntity			= true,
	removeEntity		= true,
	removeOldestEntity	= true,
	clearOverflow		= true,
}

function execute(sender, command, action, ...)
	local player = Player(sender)
	local faction = player
	local name = "Entity Loader"
	
	local messageType = 0
	
	if not actions[action] then
		player:sendChatMessage(name, messageType, "Usage:\n\ngetEntities\ngetEntity shipname\nsearchEntities fieldname keyword\naddEntity shipname\nremoveEntity shipname\nremoveOldestEntity\n\nExample: /entityLoader addEntity shipname")
		
		return
	end

	local ship
	if player.craftIndex then
		ship = Entity(player.craftIndex)
	end
	
    if ship and ship.factionIndex and ship.factionIndex == player.allianceIndex then
		faction = player.alliance
	end

	local err, ret
	
	if faction.isPlayer then
		err, ret = faction:invokeFunction("EntityLoader.lua", action, ...)

		if err == 0 then
			--player:sendChatMessage(name, messageType, action .. " executed successfully")
			
			if action == "getEntities" or action == "searchEntities" then
				for i,e in pairs(ret) do
					player:sendChatMessage(name, messageType, i)
				end
			elseif action == "removeEntity" or action == "removeOldestEntity" then
				player:sendChatMessage(name, messageType, ret)
			else
				if type(ret) == "table" then -- we have a result
					player:sendChatMessage(name, messageType, ret.name)
				elseif ret then
					player:sendChatMessage(name, messageType, "Command executed successfully")				
				else
					player:sendChatMessage(name, messageType, "Command failed")
				end
			end
			
			-- Todo: print output
			-- print(ret) -- pseudo code, we can have any type of object
		end
	else
		err = invokeFactionFunction(faction.index, true, "EntityLoader.lua", action, ...)

		if err == 0 then
			player:sendChatMessage(name, messageType, "Command executed successfully")
		end
	end
	

	if err ~= 0 then
		player:sendChatMessage(name, messageType, "Error, could not run command. Error code: " .. err)
	end
	
end