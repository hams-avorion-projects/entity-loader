package.path = package.path .. ";data/scripts/lib/?.lua"



function execute(sender, command, x, y, ...)
	local name = "Entity Loader"
	local text = "Sector " .. x .. ":" .. y .. " is"
	
	if Galaxy():keepSector(x, y, 0) then
		text = text .. " simulated"
	else
		text = text .. " NOT simulated"
	end

	Player():sendChatMessage(name, ChatMessageType.Information, text)
end