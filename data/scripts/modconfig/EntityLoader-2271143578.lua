return {
	{ name ="NumMaxEntities",					value = 5,		description = "Maximum amount of entities each player can have." },
	{ name ="KeepAliveTime",					value = 60,		description = "Time in seconds a sector will be kept updated." },
	{ name ="KeepAliveInterval",				value = 30,		description = "Minimum interval entities will be kept alive regularly. Should be lower then KeepAliveTime." },
	{ name ="UpdateInterval",					value = 3,		description = "Scripts update interval. Should be lower then KeepAliveInterval / NumMaxEntities." },
}